<?php

if ( !defined('AREA') ) { die('Access denied'); }

use Tygh\Registry;

function fn_geo_ip_get_location ($ip_address = "", $include_hidden = false) {

    $ip_location = array();
    
    //unset($_SESSION['user_location']);
    //TODO: first time dialogbox setting - displays welcome dialog with ask of location
    $get_service_data = Registry::get('addons.geo_ip.use_ipgeobase_service');
    $cheapest_shipping = Registry::get('addons.geo_ip.show_only_cheapest_shipping');
    //trying to get data from ipgeobase.ru service
    if ( empty($_SESSION['user_location']['city_name']) && $get_service_data == 'Y' ) {
        //there is no data in session
        $ip_address = $ip_address?$ip_address:$_SERVER['REMOTE_ADDR'];
        $hostname = 'http://ipgeobase.ru:7020/geo?ip='.$ip_address;
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $hostname);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $out = curl_exec($curl);
        curl_close($curl);

        $arr_out = fn_geo_ip_xmlstr_to_array($out);

        foreach ($arr_out['ip'] as $k => $v) 
            if ($k != '@attributes') $ip_location[$k] = $v;
            else $ip_location['ip'] = $v['value'];
            
        if ( empty($ip_location['city']) ) {
            //cant get data from ipgeobase.ru

            //TODO: get data from local maxmid files 
            //$ip_location = fn_geo_ip_get_location_maxmid();
            $ip_location = fn_geo_ip_get_default_location();
        } else {
            //сопоставляем имена states/region и их коды в базе cs-cart
            $states_condition = $include_hidden ? "WHERE a.status <> 'D'" : "WHERE a.status = 'A'";
            $states_condition .= " AND a.country_code = '".$ip_location['country']."' AND b.state = '".$ip_location['region']."'";
            $states_join_description = "LEFT JOIN ?:state_descriptions as b ON a.state_id = b.state_id ";
            $states = db_get_hash_array("SELECT a.state_id, a.code, b.state FROM ?:states AS a ?p", 'state_id', $states_join_description.$states_condition);
            $state = array_pop($states);

            $ip_location['country_name'] = fn_get_country_name($ip_location['country']);
            $ip_location['state_name'] = fn_get_state_name($state['code'], $ip_location['country']);
            $ip_location['cart_state_code'] = $state['code'];
            $ip_location['cart_state_id'] = $state['state_id'];
            $ip_location['city_name'] = $ip_location['city'];
        }
        
        $_SESSION['user_location'] = $ip_location;
    }  elseif ( !empty($_SESSION['user_location']) ) {
        //there is data on session
        $ip_location = array (
            'country' => $_SESSION['user_location']['country'],
            'district' => $_SESSION['user_location']['district'],
            'state' => $_SESSION['user_location']['state'],
            'cart_state_code' => $_SESSION['user_location']['cart_state_code'],
            'cart_state_id' => $_SESSION['user_location']['cart_state_id'],
            'city_name' => $_SESSION['user_location']['city_name'],
            'country_name' => $_SESSION['user_location']['country_name'],
            'state_name' => $_SESSION['user_location']['state_name'],
            'city' => $_SESSION['user_location']['city'],
        );
    } elseif ( $get_service_data == 'N' ) {
        //settings tels get data only from local maxmid file
        //TODO: get data from local maxmid files 
        $ip_location = fn_geo_ip_get_default_location();
        $_SESSION['user_location'] = $ip_location;
    }
   
    if ( empty($ip_location['city_name']) ) {
        $ip_location = fn_geo_ip_get_default_location();
        $_SESSION['user_location'] = $ip_location;
    }
    if (strpos($_SERVER['QUERY_STRING'], 'checkout.checkout') === false) {
        $_SESSION['cart']['user_data']['b_city'] = $_SESSION['user_location']['city_name'];
        $_SESSION['cart']['user_data']['s_city'] = $_SESSION['user_location']['city_name'];
        $_SESSION['cart']['user_data']['b_state'] = $_SESSION['user_location']['cart_state_code'];
        $_SESSION['cart']['user_data']['s_state'] = $_SESSION['user_location']['cart_state_code'];
        $_SESSION['cart']['user_data']['b_state_descr'] = $_SESSION['user_location']['state_name'];
        $_SESSION['cart']['user_data']['s_state_descr'] = $_SESSION['user_location']['state_name'];
        $_SESSION['cart']['user_data']['b_country'] = $_SESSION['user_location']['country'];
        $_SESSION['cart']['user_data']['s_country'] = $_SESSION['user_location']['country'];
        $_SESSION['cart']['user_data']['b_country_descr'] = $_SESSION['user_location']['country_name'];
        $_SESSION['cart']['user_data']['s_country_descr'] = $_SESSION['user_location']['country_name'];
    }
    $ip_location['display_cities'] = unserialize(GEO_IP_CITIES_LIST);
    $loc_test = array('city' => $ip_location['city_name'], 'state' => $ip_location['cart_state_code'], 'country' => $ip_location['country']);
    $ip_location['shipping_data'] = fn_geo_ip_get_shipping_taxes($loc_test);
    $_SESSION['user_location']['shipping_data'] = $ip_location['shipping_data'];
    return $ip_location;
}


/**
 * convert xml string to php array - useful to get a serializable value
 *
 */
function fn_geo_ip_xmlstr_to_array($xmlstr) {
    $doc = new DOMDocument();
    $doc->loadXML($xmlstr);
    return fn_geo_ip_domnode_to_array($doc->documentElement);
}
function fn_geo_ip_domnode_to_array($node) {
    $output = array();
    switch ($node->nodeType) {
        case XML_CDATA_SECTION_NODE:
        case XML_TEXT_NODE:
            $output = trim($node->textContent);
            break;
        case XML_ELEMENT_NODE:
            for ($i=0, $m=$node->childNodes->length; $i<$m; $i++) {
                $child = $node->childNodes->item($i);
                $v = fn_geo_ip_domnode_to_array($child);
                if(isset($child->tagName)) {
                    $t = $child->tagName;
                    if(!isset($output[$t])) {
                        $output[$t] = array();
                    }
                    $output[$t][] = $v;
                }
                elseif($v) {
                    $output = (string) $v;
                }
            }
            if(is_array($output)) {
                if($node->attributes->length) {
                    $a = array();
                    foreach($node->attributes as $attrName => $attrNode) {
                        $a[$attrName] = (string) $attrNode->value;
                    }
                    $output['@attributes'] = $a;
                }
                foreach ($output as $t => $v) {
                   if(is_array($v) && count($v)==1 && $t!='@attributes') {
                        $output[$t] = $v[0];
                   }
                }
            }
           break;
    }
    return $output;
}

function fn_geo_ip_get_location_maxmid () {
    
    static $geoipcity_loaded = false;
        
    if ($geoipcity_loaded == false) {
        include(DIR_ADDONS."geo_ip/GeoIpBase/interface/geoipcity.inc");
        include(DIR_ADDONS."geo_ip/GeoIpBase/interface/geoip.inc");
        $geoipcity_loaded = true;
    }

    include(DIR_ADDONS."geo_ip/GeoIpBase/interface/geoipregionvars.php");

    $gi = geoip_open(DIR_ADDONS."geo_ip/GeoIpBase/GeoIPCity.dat",GEOIP_STANDARD);
    $record = (array) geoip_record_by_addr($gi, $_SERVER['REMOTE_ADDR']);
    $record['region_name'] = $GEOIP_REGION_NAME[$record['country_code']][$record['region']];
    geoip_close($gi);
    //geoip_region_name_by_code($country_code, $region_code);
    return fn_geo_ip_prepare_maxmid_data( $record );
}

function fn_geo_ip_prepare_maxmid_data ($maxmid_record) {
    
    $cs_cart_state_data = db_get_row("SELECT a.state_id, a.code, b.state FROM ?:states AS a LEFT JOIN ?:state_descriptions AS b 
        ON a.state_id = b.state_id WHERE 
        a.country_code = '". $maxmid_record['country_code']."' 
        AND b.state LIKE '%".$maxmid_record['region_name']."%'");
    //translation of country and region (if it exists in cs-cart db)
    $maxmid_record['country_name'] = fn_get_country_name($maxmid_record['country_code']);
    $maxmid_record['region_name'] = fn_get_state_name($cs_cart_state_data['code'], $maxmid_record['country_code']);
    //for change location view
    $maxmid_record['country'] = $maxmid_record['country_code'];
    $maxmid_record['cart_state_code'] = $cs_cart_state_data['code'];
    $maxmid_record['cart_state_id'] = $cs_cart_state_data['state_id'];
    //city translation (there isnt at cs-cart db)
    $gm_city_name = fn_geo_ip_gm_translate_city_name($maxmid_record['city']);
    if ( $gm_city_name ) $maxmid_record['city'] = $gm_city_name;
    //var_dump($gm_city_name);
    //fn_translate_city_name($maxmid_record['city']);
    return $maxmid_record;
}

function fn_geo_ip_gm_translate_city_name ($city_en, $to_lang = CART_LANGUAGE) {
    
    $url = 'http://maps.googleapis.com/maps/api/geocode/json?address='.$city_en.'&sensor=false';
    $url = str_replace(' ', '%20', $url);
    
    $user = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1) Opera 8.50')";
    $curl = curl_init();
    $gm_json = '';
    //TODO: expand with other langs
    if ( $to_lang == 'en' )
        $header[] = "Accept-Language: en-us,en;";
    elseif ( $to_lang == 'ru' )
        $header[] = "Accept-Language: ru-ru,ru;";
    if( $curl ) {
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //for https connection
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_USERAGENT, $user);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $gm_json = curl_exec($curl);
        //$return_html = $html->load($out);
        curl_close($curl);
    } else return false;
    
    $gm_array = json_decode($gm_json, true);
    
    return $gm_array['results'][0]['address_components'][0]['long_name'];
}

// gets default location from cs-cart settings if all methods have had no result
function fn_geo_ip_get_default_location ($to_lang = CART_LANGUAGE) {
    $ip_location = array('city_name' => strval(Registry::get('settings.General.default_city')));
    $ip_location['country_name'] = db_get_field("SELECT country  FROM ?:country_descriptions WHERE code = ?s AND lang_code = ?s", strval(Registry::get('settings.General.default_country')), $to_lang);
    $ip_location['country'] = strval(Registry::get('settings.General.default_country'));
    $region_condition = " WHERE states.code = '". strval(Registry::get('settings.General.default_state')) . "'";
    $region_condition .= " AND state_descriptions.lang_code = '" . strval($to_lang) . "'";
    $region_join = "INNER JOIN ?:state_descriptions as state_descriptions ON state_descriptions.state_id = states.state_id";
    $cs_cart_state_data = db_get_row("SELECT state_descriptions.state, states.code, states.state_id FROM ?:states as states " . $region_join . $region_condition);
    $ip_location['state_name'] = $cs_cart_state_data['state'];
    $ip_location['cart_state_code'] = $cs_cart_state_data['code'];
    $ip_location['cart_state_id'] = $cs_cart_state_data['state_id'];
    return $ip_location;
}

/*Gets minimum shipping cost for visitors destination*/
function fn_geo_ip_get_shipping_taxes($location) {
    $cheapest_shipping = Registry::get('addons.geo_ip.show_only_cheapest_shipping');
    $display_full_delivery = Registry::get('addons.geo_ip.calc_delivery_with_available_in_detail');
    $group_by_regions = Registry::get('addons.geo_ip.group_by_regions_in_delivery_block');
    $destination = fn_geo_ip_get_available_destination($location);
    $product = Registry::get('view')->getTemplateVars('product');

    $ship_rates_join = " INNER JOIN ?:shipping_descriptions as shd ON sh.shipping_id = shd.shipping_id ";
    $ship_rates_join .= " INNER JOIN ?:shipping_rates as shr ON sh.shipping_id = shr.shipping_id ";
    $ship_get_string = "SELECT sh.shipping_id as shipping_id, shd.shipping as shipping_name, shd.delivery_time as shipping_time, sh.geo_ip_shipping_time as shipping_period, sh.geo_ip_destination_descr as destination_descr, shr.rate_value as shipping_rate,shr.destination_id as destination_id FROM ?:shippings as sh ";

    if (empty($destination)) {
        $ship_rates = fn_geo_ip_create_dummy_array();
    } else {
        $destination_str = "";
        foreach ($destination as $id => $dest_id) {
            $destination_str .= $dest_id[0];
            $destination_str .= ",";
        }
        $destination_str = rtrim($destination_str, ",");
        $ship_rates_condition = " WHERE sh.status = 'A' AND sh.geo_ip_shipping_block = 'Y' AND shr.destination_id IN (" . $destination_str . ") ORDER BY sh.position";
        $ship_rates =  db_get_hash_array($ship_get_string . $ship_rates_join . $ship_rates_condition, 'shipping_id');
        if (empty($ship_rates)) {
            $ship_rates = fn_geo_ip_create_dummy_array();
        }
    }
    $rates = $shippings = array();
    $rates_keys = array_keys($ship_rates);

    $always_ship_rates_condition = " WHERE sh.geo_ip_always_display = 'Y'";
    $always_ship_rates = db_get_hash_array($ship_get_string . $ship_rates_join . $always_ship_rates_condition, 'shipping_id');

    $ship_rates = array_replace($ship_rates,$always_ship_rates);
    foreach ($ship_rates as $ship_rate => $options) {
        $rates_options = unserialize($options['shipping_rate']);
        $shippings[$ship_rate] = $options;
        if (empty($rates_options)) continue;
        foreach ($rates_options as $rate_type => $rate_options) {
            $shippings[$ship_rate]['shipping_cost'] = $rate_options[0]['value'];
            if (in_array($ship_rate, $rates_keys)) {
                $rates[$ship_rate] = $shippings[$ship_rate]['shipping_cost'];
            }
        }
    }
    if ($product) {
        array_walk($shippings, 'fn_geo_ip_get_avail_period', array('product' => $product, 'display_full_delivery' => $display_full_delivery));
    }

    if (($cheapest_shipping == 'Y')&&(!empty($rates))) {
        $min_rate_id = array_search(min($rates), $rates);
        $min_shipping = array($min_rate_id => $shippings[$min_rate_id]);
        $shippings = array_replace($min_shipping,array_intersect_key($shippings, $always_ship_rates));
    }
    
    $shippings = array_diff($shippings, array(''));
//    fn_print_r(fn_geo_ip_group_by_value($shippings,'destination_descr', $location['city']));
    if ($group_by_regions == 'Y') {
        $shippings = fn_geo_ip_group_by_value($shippings,'destination_descr', $location['city']);
    }
    return  $shippings;

}

/*Compares received destination to destionations in location tables*/
function fn_geo_ip_get_available_destination($location)
{
    $result = false;

    $country = !empty($location['country']) ? $location['country'] : '';
    $state = !empty($location['state']) ? $location['state'] : '';
    $zipcode = !empty($location['zipcode']) ? $location['zipcode'] : '';
    $city = !empty($location['city']) ? $location['city'] : '';
    $address = !empty($location['address']) ? $location['address'] : '';

    if (!empty($country)) {

        $state_id = fn_get_state_id($state, $country);

        $condition = '';
        if (AREA == 'C') {
            $condition .= fn_get_localizations_condition('localization');

            if (!empty($condition)) {
                $condition = db_quote('OR (1 ?p)', $condition);
            }
        }


          $__dests = db_get_array("SELECT a.* FROM ?:destination_elements as a LEFT JOIN ?:destinations as b ON b.destination_id = a.destination_id WHERE b.status = 'A' ?p", $condition);

        $destinations = array();
        foreach ($__dests as $k => $v) {
            $destinations[$v['destination_id']][$v['element_type']][] = $v['element'];
        }

        $concur_destinations = array();

        foreach ($destinations as $dest_id => $elm_types) {
            // Significance level. The more significance level means the most amount of coincidences
            $significance = 0;
            $dest_countries = !empty($elm_types['C']) ? $elm_types['C'] : array();
            foreach ($elm_types as $elm_type => $elms) {
                // Check country
                if ($elm_type == 'C') {
                    $suitable = fn_check_element($elms, $country);
                    if ($suitable == false) {
                        break;
                    }

                    $significance += 1 * (1 / count($elms));
                }

                // Check state
                if ($elm_type == 'S') {
                    // if country is in destanation_countries and it haven't got states,
                    // we suppose that destanation cover all country
                    if (!in_array($country, $dest_countries) || fn_get_country_states($country)) {
                        $suitable = fn_check_element($elms, $state_id);
                        if ($suitable == false) {
                            break;
                        }
                    } else {
                        $suitable = true;
                    }
                    $significance += 2 * (1 / count($elms));
                }
                // Check city
                if ($elm_type == 'T') {
                    $suitable = fn_check_element($elms, $city, true);
                    if ($suitable == false) {
                        break;
                    }
                    $significance += 3 * (1 / count($elms));
                }
                // Check zipcode
                if ($elm_type == 'Z') {
                    $suitable = fn_check_element($elms, $zipcode, true);
                    if ($suitable == false) {
                        break;
                    }
                    $significance += 4 * (1 / count($elms));
                }

                // Check address
                if ($elm_type == 'A') {
                    $suitable = fn_check_element($elms, $address, true);
                    if ($suitable == false) {
                        break;
                    }
                    $significance += 5 * (1 / count($elms));
                }
            }

            $significance = number_format($significance, 4, '.', '');

            if ($suitable == true) {
                $concur_destinations[$significance][] = $dest_id;
            }
        }

        if (!empty($concur_destinations)) {
            ksort($concur_destinations, SORT_NUMERIC);
        }
    }

    return $concur_destinations;

}

/*Functions gets data from users and trie to set his destination correctly*/
function fn_geo_ip_change_location($geo_data)
{
        $location = array();
        $right_city = false;
        if (!empty($geo_data['city_name'])) {
            
            $city_condition = " WHERE rc.status = 'A' AND rcd.city = '".$geo_data['city_name']."' AND rcd.lang_code = '".CART_LANGUAGE."' ";
            $city_join = " INNER JOIN ?:rus_cities AS rc ON rc.city_id=rcd.city_id ";
            $city = db_get_row("SELECT rc.city_id as city, rcd.city as city_name FROM ?:rus_city_descriptions AS rcd ".$city_join.$city_condition);
            if (!empty($city)) {
                $right_city = true;
                $location = $city;
            } else {
                $right_city = false;
                $location['city_name'] = $geo_data['city_name'];
            }
        }
        $state = array();
        $right_state = false;
        $state_join = " INNER JOIN ?:states AS s ON s.state_id=sd.state_id ";
        $state_request = "SELECT s.state_id as state, sd.state as state_name, s.code as cart_state_code FROM ?:state_descriptions AS sd ";
        if (!empty($geo_data['state_name'])) {
            $state_condition = " WHERE s.status = 'A' AND sd.state = '".$geo_data['state_name']."' AND sd.lang_code = '".CART_LANGUAGE."' ";
            $state = db_get_row($state_request.$state_join.$state_condition);
        }    
        if (empty($state) && $right_city) {
            // try to get state if city was found but state was incorrect
            $right_state = false;
            $state_join .= " INNER JOIN ?:rus_cities AS rc ON s.code=rc.state_code ";
            $state_condition = " WHERE s.status = 'A' AND rc.city_id = '".$location['city']."' AND sd.lang_code = '".CART_LANGUAGE."' ";
            $state = db_get_row($state_request.$state_join.$state_condition);
        }
        if (!empty($state)) {
            $right_state = true;
            $location = array_merge($location,$state);
            $location['cart_state_id'] = $state['state'];
        } else {
            $location['state_name'] = empty($geo_data['state_name'])?"":$geo_data['state_name'];
            $right_state = false;
        }
        
        $country = array();
        $country_join = " INNER JOIN ?:countries AS c ON c.code=cd.code ";
        $country_request = "SELECT c.code as country, cd.country as country_name FROM ?:country_descriptions AS cd "; 
        if (!empty($geo_data['country_name'])) {
            $country_condition = " WHERE c.status = 'A' AND cd.country = '".$geo_data['country_name']."' AND cd.lang_code = '".CART_LANGUAGE."' ";
            $country = db_get_row($country_request.$country_join.$country_condition);
        }
        if (empty($country) && $right_state) {
            // try to get country if state was found but country was incorrect
            $country_join .= " INNER JOIN ?:states AS s ON s.country_code=c.code ";
            $country_condition = " WHERE c.status = 'A' AND s.state_id = '".$location['cart_state_id']."' AND cd.lang_code = '".CART_LANGUAGE."' ";
            $country = db_get_row($country_request.$country_join.$country_condition);
        }
        if (!empty($country)) {
            $location = array_merge($location,$country);
        } else {
            $location['country_name'] = empty($geo_data['country_name'])?"":$geo_data['country_name'];
        }
    if (empty($state)||empty($country)||empty($city)) {
        $location['failed'] = true;
    } else {
        $location['failed'] = false;
    }
    return $location;
}

/*Prepares data for shipping block - calculates delivery and shipping periods*/
function fn_geo_ip_get_avail_period(&$item, $key, $parameters) {
    if (($parameters['product']['amount'] < 1)&&($parameters['display_full_delivery'] == 'Y')) {
        $avail = $parameters['product']['avail_since'] - time();
        $item['shipping_period'] = ($avail > 0)?ceil($avail/86400) + $item['shipping_period']: $item['shipping_period'];
    } 
    $item['shipping_period'] = strval($item['shipping_period']) . ' ' . fn_geo_ip_number($item['shipping_period']);
}


function fn_geo_ip_number($n, $titles = array('день','дня','дней')) {
  $cases = array(2, 0, 1, 1, 1, 2);
  return $titles[($n % 100 > 4 && $n % 100 < 20) ? 2 : $cases[min($n % 10, 5)]];
}

/*Extracts shipping array and groups its values by shipping types*/
function fn_geo_ip_group_by_value($array, $key, $default) {
    $res_array = array();
    foreach ($array as $arr_key => $options) {
        $value = (empty($options[$key]))?$default:$options[$key];
        if (array_key_exists($value, $res_array)) {
            $res_array[$value][] = $array[$arr_key];
        } else {
            $res_array[$value] = array($array[$arr_key]);
        }
    }
    return $res_array;
}

function fn_geo_ip_get_products_post(&$products, $parameters, $lang_code) {
    $show_delivery_in_product_list = Registry::get('addons.geo_ip.show_delivery_in_product_list');
    if ($show_delivery_in_product_list == 'Y') {
        foreach ($products as $product => &$options) {
            $avail = (empty($options['avail_period']))? ceil(($product['avail_since'] - time())/86400):$options['avail_period'];
            $options['avail_period_text'] = ($avail > 0)? $avail . ' ' . fn_geo_ip_number($avail) : '';
        }
    }
}

function fn_geo_ip_get_product_data_post(&$product_data, $items, $lang_code) {
    $show_delivery_in_product_list = Registry::get('addons.geo_ip.show_delivery_in_product_list');
    if ($show_delivery_in_product_list == 'Y') {
        $avail = (empty($product_data['avail_period']))? ceil(($product_data['avail_since'] - time())/86400):$product_data['avail_period'];
        $product_data['avail_period_text'] = ($avail > 0)? $avail . ' ' . fn_geo_ip_number($avail) : '';        
    }
}

/*Creates dummy array - it will be displayed to user on location failure*/
function fn_geo_ip_create_dummy_array() {
    $shippings = array();
    $shipping_failed_text = Registry::get('addons.geo_ip.shipping_failed_text');
    $show_failed_delivery = Registry::get('addons.geo_ip.show_on_delivery_failure');
    if ($show_failed_delivery == 'show_delivery_failure_as_delivery') {
        $shippings[] = array('shipping_id' => '0', 'failed_text' => $shipping_failed_text, 'destination_descr' => '');
    }
    return $shippings;
}
?>
