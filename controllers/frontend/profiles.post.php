<?php
/*
 * This snippet fills the contry/region/city field in profile form
 * with data from GeoIp Location module
 * if the data was not filled by user manually
 */

use Tygh\Registry;

if ($mode == 'update') {
    //get current user data
    $user_data = Tygh::$app['view']->getTemplateVars('user_data');
    
    if ( empty($user_data['s_state']) || empty($user_data['s_city'])) {
        //не задана область и город (страна задается автоматически cs-cart)
        //значит пользователь их еще не вводил
        
        $user_location = $_SESSION['user_location']; 
        
        $user_data['b_country'] = $user_location['country'];
        $user_data['s_country'] = $user_location['country'];
        $user_data['b_state'] = $user_location['cart_state_code'];
        $user_data['s_state'] = $user_location['cart_state_code'];
        $user_data['b_city'] = $user_location['city_name'];
        $user_data['s_city'] = $user_location['city_name'];

        Tygh::$app['view']->assign('user_data', $user_data);
    }

}

?>
