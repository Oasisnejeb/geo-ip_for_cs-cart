<?php	 	

use Tygh\Registry;

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {  
    
    if ($mode == 'update') {
        $return_url = !empty($_REQUEST['return_url']) ? $_REQUEST['return_url'] : '';
 
       if (!empty(array_filter($_POST['geo_data'],"strlen"))) {
            $location = fn_geo_ip_change_location($_POST['geo_data']);
            $_SESSION['user_location'] = $location;
            if ($location['failed'] != true) {
			    $_SESSION['cart']['user_data']['b_city'] = $_SESSION['user_location']['city_name'];
			    $_SESSION['cart']['user_data']['s_city'] = $_SESSION['user_location']['city_name'];
			    $_SESSION['cart']['user_data']['b_state'] = $_SESSION['user_location']['cart_state_code'];
			    $_SESSION['cart']['user_data']['s_state'] = $_SESSION['user_location']['cart_state_code'];
			    $_SESSION['cart']['user_data']['b_state_descr'] = $_SESSION['user_location']['state_name'];
			    $_SESSION['cart']['user_data']['s_state_descr'] = $_SESSION['user_location']['state_name'];
			    $_SESSION['cart']['user_data']['b_country'] = $_SESSION['user_location']['country'];
			    $_SESSION['cart']['user_data']['s_country'] = $_SESSION['user_location']['country'];
			    $_SESSION['cart']['user_data']['b_country_descr'] = $_SESSION['user_location']['country_name'];
			    $_SESSION['cart']['user_data']['s_country_descr'] = $_SESSION['user_location']['country_name'];
			}
        }

        return array(CONTROLLER_STATUS_OK, $return_url);
    }
}


?>
