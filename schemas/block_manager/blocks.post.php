<?php

$schema['geo_ip'] =  array (
		'content' => array (
			'location' => array (
				'type' => 'function',
				'function' => array('fn_geo_ip_get_location'),
			),
		),
                'templates' => array(
                            'addons/geo_ip/blocks/plain_data.tpl' => array(),
                            'addons/geo_ip/blocks/breadcrumps_location.tpl' => array(),
                            'addons/geo_ip/blocks/location_change.tpl' => array(),
                            'addons/geo_ip/blocks/shipping_data.tpl' => array(),

                ),
                'wrappers' => 'blocks/wrappers',
        );

return $schema;
