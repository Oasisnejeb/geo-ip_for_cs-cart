<?php

if ( !defined('BOOTSTRAP') ) { die('Access denied'); }

fn_register_hooks(
	'get_products_post',
	'get_product_data_post'
);

?>
